package br.com.finalelite.bots.authenticator;

import br.com.finalelite.bots.authenticator.utils.Config;
import br.com.finalelite.bots.authenticator.utils.Database;
import com.google.common.base.Charsets;
import com.google.common.io.Files;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.Getter;
import lombok.val;
import lombok.var;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;
import net.dv8tion.jda.core.MessageBuilder;
import net.dv8tion.jda.core.entities.ChannelType;
import net.dv8tion.jda.core.events.guild.GuildJoinEvent;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.events.message.react.MessageReactionAddEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

import javax.security.auth.login.LoginException;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;

public class Main extends ListenerAdapter {

    @Getter
    private static JDA jda;
    @Getter
    private static Config config;
    @Getter
    private static Database db;
    @Getter
    private static final String pasteURL = "https://paste.fedoraproject.org/api/paste/submit";

    public static void main(String[] args) {
        val file = new File("config.json");
        if (!file.exists()) {
            try {
                file.createNewFile();
                val defaultConfig = Config.builder()
                        .token("your token")
                        .ownerId("123")
                        .sqlAddress("localhost")
                        .sqlPort(3306)
                        .sqlUsername("root")
                        .sqlPassword("1234")
                        .sqlDatabase("database")
                        .build();
                val writer = Files.newWriter(file, Charsets.UTF_8);
                Gson gson = new GsonBuilder().setPrettyPrinting().create();
                writer.write(gson.toJson(defaultConfig));
                writer.close();
                System.out.println("Default config file created, please, configure and run the bot again.");
                System.exit(0);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try {
            config = new Gson().fromJson(String.join("\n", Files.readLines(file, Charsets.UTF_8)), Config.class);
        } catch (IOException e) {
            System.out.println("Cannot load the config file.");
            e.printStackTrace();
            System.exit(-1);
        }

        db = new Database(config.getSqlAddress(), config.getSqlPort(), config.getSqlUsername(), config.getSqlPassword(), config.getSqlDatabase());
        try {
            db.connect();
            System.out.println("Connected to MySQL.");
        } catch (SQLException | ClassNotFoundException e) {
            System.out.println("Cannot connect to database.");
            e.printStackTrace();
            System.exit(-3);
        }

        try {
            jda = new JDABuilder(config.getToken()).build().awaitReady();
            System.out.println("Logged.");
            if (jda.getGuilds().size() == 0)
                System.out.printf("Invite-me for a server: https://discordapp.com/oauth2/authorize?client_id=%s&permissions=8&scope=bot%n", jda.getSelfUser().getId());
            else if (jda.getGuilds().size() > 1) {
                shutdown(String.format("The bot is in %d guilds. For security, the bot only run in the official guild.", jda.getGuilds().size()));
            }
            jda.addEventListener(new Main());
        } catch (InterruptedException | LoginException e) {
            System.out.println("Cannot login.");
            e.printStackTrace();
            System.exit(-2);
        }

        Runtime.getRuntime().addShutdownHook(new Thread(() -> shutdown("Exited by user")));
    }

    public static void shutdown(String reason) {
        System.out.printf("Shutting down. %s%n", reason);
        val pv = Main.getJda().getUserById(Main.getConfig().getOwnerId()).openPrivateChannel().complete();
        pv.sendMessage(String.format(":warning: Shutting down your bot: %s", reason)).complete();        
        jda.shutdownNow();
    }

    @Override
    public void onGuildJoin(GuildJoinEvent event) {
        if (jda.getGuilds().size() == 0)
            return;
        shutdown(String.format("The bot is in %d guilds. For security, the bot only run in the official guild.", jda.getGuilds().size()));
    }

    @Override
    public void onMessageReactionAdd(MessageReactionAddEvent event) {
        val channel = event.getChannel();
        val user = event.getUser();
        val message = channel.getMessageById(event.getMessageId()).complete();
        val author = message.getAuthor();

        if (channel.getType() == ChannelType.PRIVATE && author.getId().equals(jda.getSelfUser().getId()) && message.getContentRaw().startsWith(":warning:")) {
            if(user.getId().equals(jda.getSelfUser().getId()))
                return;

            val rawMessage = message.getContentRaw().split(" ");
            var ip = rawMessage[rawMessage.length - 1]; // get last "world"
            ip = ip.substring(0, ip.length() - 1); // remove the period

            val nick = db.getNickFromDiscordID(user.getId());
            if (nick == null) {
                channel.sendMessage(":x: 1Seu Discord não está ligado a uma conta de Staff. Use `!conta <nick>` para ligar.").complete();
                return;
            }

            db.updateIp(nick, ip);

            jda.getGuilds().get(0).getTextChannelsByName("autenticador-log-adm", false).get(0)
                    .getMessageById(db.getPublicMessageIdFromPrivateMessageId(user.getId(), message.getId())).complete().delete().complete();

            db.removePrivateWarn(user.getId(), message.getId());

            channel.sendMessage(new MessageBuilder(String.format(":white_check_mark: IP da conta %s alterado para %s", nick, ip)).build()).complete();
            System.out.printf("%s (%s) ligou a conta com o IP %s.%n", user.getName(), user.getId(), ip);

        }
    }

    @Override
    public void onMessageReceived(MessageReceivedEvent event) {
        val message = event.getMessage();
        val channel = message.getChannel();
        val author = message.getAuthor();
        val raw = message.getContentRaw();

        if (!(author.isBot() && (channel.getType() == ChannelType.TEXT) && jda.getGuilds().get(0).getTextChannelById(channel.getId()) != null) && author.getMutualGuilds().size() != 1) {
            channel.sendMessage("Algo de errado não está certo :thiking:.").queue();
            return;
        }

        if (channel.getType() == ChannelType.TEXT && channel.getId().equals("518183777248346122") && author.isBot() && message.getMentionedUsers().size() == 1 && author.getName().equalsIgnoreCase("Watchdog")) {
            val target = message.getMentionedUsers().get(0);
            val pv = target.openPrivateChannel().complete();

            val pvMessage = pv.sendMessage(message).complete();
            pvMessage.addReaction("✅").complete();

            db.savePrivateWarn(target.getId(), pvMessage.getId(), message.getId());

            return;
        }

        if (raw.toLowerCase().startsWith("!ip")) {
            if (!raw.contains(" ")) {
                channel.sendMessage(":x: Use `!ip <ip>`. Veja seu IP aqui https://api.ipify.org/.").queue();
                return;
            }

            val ip = raw.split(" ")[1];
            if (!ip.matches("^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$")) {
                channel.sendMessage(":x: IP inválido. Veja seu IP aqui https://api.ipify.org/.").queue();
                return;
            }

            val nick = db.getNickFromDiscordID(author.getId());
            if (nick == null) {
                channel.sendMessage(":x: 0Seu Discord não está ligado a uma conta de Staff. Use `!conta <nick>` para ligar. Se isso for um engano, tente novamente!").queue();
                return;
            }

            db.updateIp(nick, ip);
            channel.sendMessage(":white_check_mark: IP alterado com sucesso.").queue();
            System.out.printf("%s (%s) ligou a conta com o IP %s.%n", author.getName(), author.getId(), ip);
            if (channel.getType() == ChannelType.TEXT)
                message.delete().queue();
            return;
        }

        if (raw.toLowerCase().startsWith("!conta")) {
            if (!raw.contains(" ")) {
                channel.sendMessage(":x: Use `!conta <nick>`.").queue();
                return;
            }

            val nick = raw.split(" ")[1];

            val result = db.registerDiscord(nick, author.getId());
            if (result == 0) {
                channel.sendMessage(":white_check_mark: Conta ligada com sucesso.").queue();
                System.out.printf("%s (%s) ligou a conta com o nick %s.%n", author.getName(), author.getId(), nick);
            } else if (result == 1) {
                channel.sendMessage(":x: O nick ou o Discord já está ligado a uma conta.").queue();
            } else if (result == 2) {
                channel.sendMessage(":x: O bot não conseguiu acessar o banco de dados. Uma mensagem com o erro foi enviada para o dono do bot.").queue();
            }
        } else if (raw.toLowerCase().startsWith("!remover")) {
            if (event.getGuild() == null) {
                channel.sendMessage(":x: Use esse comando no servidor da equipe.").queue();
                return;
            }
            val roles = event.getGuild().getRolesByName("Supervisor", true);
            if (roles.size() != 1) {
                channel.sendMessage(":x: Role `Supervisor` não encontrada.").queue();
                return;
            }
            if (!event.getGuild().getMember(author).getRoles().contains(roles.get(0))) {
                channel.sendMessage(":x: Você não tem a role `Supervisor`.").queue();
                return;
            }
            if (!raw.contains(" ")) {
                channel.sendMessage(":x: Use `!remover <nick>`.").queue();
                return;
            }
            val nick = raw.split(" ")[1];
            val result = getDb().removeFromDatabase(nick);

            if (result == 0) {
                channel.sendMessage(":white_check_mark: Usuário removido do banco de dados.").queue();
            } else {
                channel.sendMessage(":x: Algo deu errado. O nick está correto?").queue();
            }
        }

    }
}
