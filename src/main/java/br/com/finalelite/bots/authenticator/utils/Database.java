package br.com.finalelite.bots.authenticator.utils;

import br.com.finalelite.bots.authenticator.Main;
import com.gitlab.pauloo27.core.sql.*;
import lombok.RequiredArgsConstructor;
import lombok.val;
import net.dv8tion.jda.core.MessageBuilder;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@RequiredArgsConstructor
public class Database {

    private final String address;
    private final int port;
    private final String username;
    private final String password;
    private final String database;

    private EzSQL sql;
    private EzTable userIp;
    private EzTable discordInfo;
    private EzTable privateWarns;

    public void connect() throws SQLException, ClassNotFoundException {
        sql = new EzSQL(EzSQLType.MYSQL)
                .withAddress(address, port)
                .withDefaultDatabase(database)
                .withLogin(username, password);

        sql.registerDriver().connect();

        userIp = sql.createIfNotExists(
                new EzTableBuilder("User_ip")
                        .withColumn(new EzColumnBuilder("id", EzDataType.PRIMARY_KEY))
                        .withColumn(new EzColumnBuilder("nick", EzDataType.VARCHAR, 16, EzAttribute.UNIQUE))
                        .withColumn(new EzColumnBuilder("ip", EzDataType.VARCHAR, 15, EzAttribute.UNIQUE))
        );


        discordInfo = sql.createIfNotExists(
                new EzTableBuilder("Discord_info")
                        .withColumn(new EzColumnBuilder("id", EzDataType.PRIMARY_KEY))
                        .withColumn(new EzColumnBuilder("nick", EzDataType.VARCHAR, 16, EzAttribute.UNIQUE))
                        .withColumn(new EzColumnBuilder("discordId", EzDataType.VARCHAR, 64, EzAttribute.UNIQUE))
        );

        privateWarns = sql.createIfNotExists(
                new EzTableBuilder("Private_warns")
                        .withColumn(new EzColumnBuilder("userId", EzDataType.VARCHAR, 64))
                        .withColumn(new EzColumnBuilder("publicMessageId", EzDataType.VARCHAR, 64))
                        .withColumn(new EzColumnBuilder("privateMessageId", EzDataType.VARCHAR, 64))
        );
    }

    private int updateTries = 0;

    public synchronized void updateIp(String nick, String ip) {
        if (updateTries++ > 3) {
            Main.shutdown("ALREADY TRIED 3 TIMES TO UPDATE THE IP");
            System.exit(-3);
            return;
        }
        try {
            val stmt = sql.getConnection().prepareStatement("INSERT INTO User_ip (nick, ip) VALUES (?, ?) ON DUPLICATE KEY UPDATE nick = ?, ip = ?;");
            stmt.setString(1, nick);
            stmt.setString(2, ip);
            stmt.setString(3, nick);
            stmt.setString(4, ip);
            stmt.executeUpdate();
            updateTries = 0;
        } catch (SQLException e) {
            reconnectSQL(e);
            if (e.getMessage().startsWith("The last packet successfully received from the server was"))
                updateIp(nick, ip);
        }
    }

    private int savePrivateWarnsTries = 0;

    public synchronized void savePrivateWarn(String userId, String privateMessageId, String publicMessageId) {
        if (savePrivateWarnsTries++ > 3) {
            Main.shutdown("ALREADY TRIED 3 TIMES TO SAVE THE WARN");
            System.exit(-3);
            return;
        }
        try {
            privateWarns.insert(new EzInsert("userId, privateMessageId, publicMessageId", userId, privateMessageId, publicMessageId)).close();
            savePrivateWarnsTries = 0;
        } catch (SQLException e) {
            reconnectSQL(e);
            if (e.getMessage().startsWith("The last packet successfully received from the server was"))
                savePrivateWarn(userId, privateMessageId, publicMessageId);
        }
    }

    private int removePrivateWarnTries = 0;

    public synchronized void removePrivateWarn(String userId, String privateMessageId) {
        if (removePrivateWarnTries++ > 3) {
            Main.shutdown("ALREADY TRIED 3 TIMES TO REMOVE THE WARN");
            System.exit(-3);
            return;
        }
        try {
            privateWarns.delete(new EzDelete().where().equals("userId", userId).and().equals("privateMessageId", privateMessageId)).close();
            removePrivateWarnTries = 0;
        } catch (SQLException e) {
            reconnectSQL(e);
            if (e.getMessage().startsWith("The last packet successfully received from the server was"))
                removePrivateWarn(userId, privateMessageId);
        }
    }

    private int getPublicMessageIdTries = 0;

    public synchronized String getPublicMessageIdFromPrivateMessageId(String userId, String privateMessageId) {
        if (getPublicMessageIdTries++ > 3) {
            Main.shutdown("ALREADY TRIED 3 TIMES TO GET THE PUBLIC ID");
            System.exit(-3);
            return ""; // just to avoid ide "redundant code" warning
        }

        try (val rs = privateWarns.select(new EzSelect("publicMessageId")
                .where().equals("userId", userId).and()
                .equals("privateMessageId", privateMessageId)).getResultSet()) {
            getPublicMessageIdTries = 0;

            if (rs.next())
                return rs.getString("publicMessageId");

        } catch (SQLException e) {
            reconnectSQL(e);
            if (e.getMessage().startsWith("The last packet successfully received from the server was"))
                return getPublicMessageIdFromPrivateMessageId(userId, privateMessageId);
        }
        return null;
    }

    private int getNickTries = 0;

    public synchronized String getNickFromDiscordID(String id) {
        if (getNickTries++ > 3) {
            Main.shutdown("ALREADY TRIED 3 TIMES GET NICK FROM DISCORD ID");
            System.exit(-3);
            return null;
        }
        try (val rs = discordInfo.select(new EzSelect("nick").where().equals("discordId", id)).getResultSet()) {
            getNickTries = 0;
            if (rs.next())
                return rs.getString("nick");
            else
                return null;
        } catch (SQLException e) {
            reconnectSQL(e);
            if (e.getMessage().startsWith("The last packet successfully received from the server was"))
                return getNickFromDiscordID(id);
        }
        return null;
    }

    private int removeTries = 0;

    public synchronized byte removeFromDatabase(String nick) {
        if (removeTries++ > 3) {
            Main.shutdown("ALREADY TRIED 3 TIMES REMOVE THE USER");
            System.exit(-3);
            return -3;
        }
        try {
            val update = discordInfo.delete(new EzDelete().where().equals("nick", nick));
            userIp.delete(new EzDelete().where().equals("nick", nick)).close();
            val changes = update.getUpdatedRows();
            update.close();
            removeTries = 0;
            return (byte) (changes == 1 ? 0 : 1);
        } catch (SQLException e) {
            reconnectSQL(e);
            if (e.getMessage().startsWith("The last packet successfully received from the server was"))
                return removeFromDatabase(nick);
        }
        return 2;
    }

    private int registerTries = 0;

    public synchronized byte registerDiscord(String nick, String discordId) {
        if (registerTries++ > 3) {
            Main.shutdown("ALREADY TRIED 3 TIMES REGISTER THE DISCORD");
            System.exit(-3);
            return -123;
        }
        try {
            discordInfo.insert(new EzInsert("nick, discordId", nick, discordId)).close();
            registerTries = 0;
            return 0;
        } catch (SQLException e) {
            if (e.getMessage().startsWith("Duplicate entry"))
                return 1;
            reconnectSQL(e);
            if (e.getMessage().startsWith("The last packet successfully received from the server was"))
                return registerDiscord(nick, discordId);
        }
        return 2;
    }

    public void handleException(Exception e) {
        if (e.getMessage().startsWith("The last packet successfully received from the server was"))
            return;

        val pv = Main.getJda().getUserById(Main.getConfig().getOwnerId()).openPrivateChannel().complete();
        val sb = new StringBuilder();
        sb.append("**Look, a poem:**\n");
        sb.append(e.getMessage()).append("\n");
        sb.append(e.getCause()).append("\n");
        Arrays.stream(e.getStackTrace()).forEach(stackTraceElement -> sb.append(stackTraceElement.toString()).append("\n"));
        val lines = Arrays.asList(sb.toString().split("\n"));
        val times = lines.size() / 10;
        IntStream.range(0, times == 0 ? 1 : times).forEach(time -> {
            val tempLines = lines.stream().skip(time * 10).limit(10).collect(Collectors.toCollection(ArrayList::new));
            tempLines.add(time == 0 ? 1 : 0, "```java");
            tempLines.add("```");
            pv.sendMessage(new MessageBuilder(String.join("\n", tempLines)).build()).complete();
        });
    }

    public void reconnectSQL(SQLException e) {
        e.printStackTrace();
        handleException(e);
        try {
            sql.connect();
        } catch (SQLException e1) {
            e1.printStackTrace();
            handleException(e1);
        }
    }
}
